import { Injectable } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';

@Injectable({
   providedIn: 'root'
})
export class RouterService {

   constructor(
      private router: Router,
   ) { }

   routeTo(path: string, params?: any) {
      this.router.navigate([path, params]);
   }

}
