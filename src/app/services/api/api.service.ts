import { Injectable } from '@angular/core';

import { Observable, from  } from 'rxjs';

import { HTTP } from '@ionic-native/http/ngx';
import { AlertController, LoadingController } from '@ionic/angular';

@Injectable({
   providedIn: 'root'
})
export class ApiService {
   api_url: string = 'http://kzone.asuscomm.com:10080';     // Update API Host Here

   public_api: PublicApi;
   secure_api: SecureApi;

   constructor(
      private http: HTTP,
      public alertController: AlertController,
      public loadingController: LoadingController,
   ) {
      this.public_api = new PublicApi(this.api_url, http);
      this.secure_api = new SecureApi(this.api_url, http);
   }

   async execute(request: Observable<any>, callback: (res: any) => void, errorHandle?: (error: any) => void) {
      const loading = await this.loadingController.create();
      await loading.present();
      await request.subscribe(
         res => {
            console.debug('result - ', res);
            loading.dismiss();
            callback(res);
         }, 
         err => {
            console.log(err);
            loading.dismiss();
            errorHandle(err);
         }
      );
   }
}

class Api {

   constructor(public api_url: string, private http: HTTP) { }

   public get(api_url: string, headers?: any): Observable<any> {
      let responseData = this.http.get(api_url, {}, headers).then(resp => JSON.parse(resp.data));
      return from(responseData);
   }

   public post(api_url: string, data: any, headers?: any): Observable<any> {
      this.http.setDataSerializer('json');
      let responseData = this.http.post(api_url, data, headers).then(resp => JSON.parse(resp.data));
      return from(responseData);
   }

}

class PublicApi extends Api {

   constructor(api_url: string, http: HTTP) {
      super(api_url += '/evcs/app', http);
   }

   // pass following to call api <url, data, header>
   getCarInfo(lang: string): Observable<any> {
      return this.post(this.api_url + '/meta', { action: 'MetaCarInfo' }, { 'Accept-Language': lang })
   }
}

class SecureApi extends Api {

   // private accInfo: any

   constructor(public api_url: string, http: HTTP) {
      super(api_url += '/evcs/app/secure', http);
   }

   // setAccInfo(accInfo: any) {
   //    this.accInfo = accInfo;
   // }

   // pass following to call api <url, data, header>
   // updateLang(lang: string) {
   //    return this.post(this.api_url + '/account', { action: 'UpdateLang', defaultLang: lang }, { csToken: this.accInfo.activeToken });
   // }
}
