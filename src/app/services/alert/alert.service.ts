import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
   providedIn: 'root'
})
export class AlertService {

   constructor(
      private translate: TranslateService,
      public alertController: AlertController
   ) { }

   async presentAlert(res: any, header?: string) {
      const message = (this.translate.currentLang == 'zh' ? res['msgZh'] : res['msgEn']);
      console.log(message);

      const alert = await this.alertController.create({
         header: header,
         message: message,
         buttons: ['OK']
      });

      await alert.present();
   }

   async presentMessage(message: string, header?: string) {
      console.log(message);

      const alert = await this.alertController.create({
         header: header,
         message: message,
         buttons: ['OK']
      });

      await alert.present();
   }
}
