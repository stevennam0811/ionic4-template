import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api/api.service';
import { AlertService } from '../../services/alert/alert.service';
import { TranslateService } from '../../../../node_modules/@ngx-translate/core';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';

@Component({
   selector: 'app-home',
   templateUrl: './home.page.html',
   styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

   id: number;

   constructor(
      private translate: TranslateService,
      private route: ActivatedRoute,
      private api: ApiService,
      private alert: AlertService
   ) { }

   ngOnInit() {
      this.route.params.subscribe((params) => {
         this.id = params['id'];
      });
   }

   ionViewWillEnter() {
      let lang: string = this.translate.currentLang;
      console.debug('Current Lang: ', lang);

      this.api.execute(this.api.public_api.getCarInfo(lang), (res: any) => {
         if (res.success) {
            this.alert.presentMessage('Get Data Success', 'Success');
         }
      }, (error: any) => {
         this.alert.presentMessage(error, 'Error');
      });
   }

}
